/*
 * config.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_CONFIG_H_
#define FUTIL_CONFIG_H_

// Optional flags and defines comes here to configure futil's code.

/** Uncomment if there is a conflict with the standard library's strtof() */
//#define FUTIL_DISABLE_STRTOF_IMPLEMENTATION

/// Uncomment these if you wont need them when building your executable with futil source code.
//#define FUTIL_DISABLE_EXCEPTION_IMPLEMENTATION
//#define FUTIL_DISABLE_PROPERTIES_IMPLEMENTATION
//#define FUTIL_DISABLE_CHRONO_IMPLEMENTATION
//#define FUTIL_DISABLE_RANDOM_IMPLEMENTATION
//#define FUTIL_DISABLE_ROUND_IMPLEMENTATION
//#define FUTIL_DISABLE_STRING_ACTIONS_IMPLEMENTATION
//#define FUTIL_DISABLE_STRING_EXTRA_OPERATORS_IMPLEMENTATION
//#define FUTIL_DISABLE_STRING_PARSE_IMPLEMENTATION
//#define FUTIL_DISABLE_STRING_SPLIT_IMPLEMENTATION
//#define FUTIL_DISABLE_SNPRINTF_IMPLEMENTATION
//#define FUTIL_DISABLE_STRINGIFY_IMPLEMENTATION
//#define FUTIL_DISABLE_STRTOX_IMPLEMENTATION
//#define FGEAL_DISABLE_STOX_IMPLEMENTATION

#endif /* FUTIL_CONFIG_H_ */
