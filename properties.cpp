/*
 * properties.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_PROPERTIES_IMPLEMENTATION

#include "properties.hpp"

#include "collection_actions.hpp"
#include "string_actions.hpp"
#include "string_split.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>

using std::map;
using std::string;
using std::vector;

/* Extracts key and value from given line and stores them in the 'key' and 'value' variables, returning true if successful.
 * If no valid key-value pair is found, then false is returned and both 'key' and 'value' variables remain unchanged. */
static bool extractKeyValueFromLine(string line, string& key, string& value)
{
	line = futil::trim(line);

	if(line.empty() or futil::starts_with(line, "#") or futil::starts_with(line, "!") or futil::starts_with(line, ";"))
		return false;

	vector<string> tokens = futil::split(line, '=');
	if(tokens.size() != 2)
	{
		tokens = futil::split(line, ':');
		if(tokens.size() != 2)
		{
			tokens = futil::split(line, ' ');
			if(tokens.size() != 2)
			{
				tokens = futil::split(line, '\t');
				if(tokens.size() != 2)
					return false;
			}
		}
	}
	const string supposedKey = futil::trim(tokens[0]);
	if(supposedKey.empty())  // if key is absent, then no valid key-value pair is available (missing only value is permitted, though)
		return false;

	key = supposedKey;
	value = futil::trim(tokens[1]);
	return true;
}

namespace futil
{
	/* Maps the specified key to the specified value in this properties object.
	 * The value can be retrieved by calling the get method with a key that is equal to the original key.
	 * Same as 'properties[key] = value', but returns the previous value of the specified key in this
	 * property list, or an empty string if it did not have one. */
	string Properties::put(const string& key, const string& value)
	{
		string oldValue = (*this)[key];
		(*this)[key] = value;
		return oldValue;
	}

	/* Searches for the property with the specified key in this properties object and returns it.
	 * If the key is not found, an empty string or an optionally provided 'defaultValue' is returned. */
	string Properties::get(const string& key, const string& defaultValue) const
	{
		map<string, string>::const_iterator lb = lower_bound(key);
		if((lb != end() and not key_comp()(key, lb->first)))
			return lb->second;
		else
			return defaultValue;
	}

	/* Searches for the property with the specified key in this properties object and returns it if found.
	 * Otherwise a std::invalid_argument exception is thrown, optionally with the given 'errorMsg'. */
	string Properties::retrieve(const string& key, const string& errorMsg) const
	{
		map<string, string>::const_iterator lb = lower_bound(key);
		if((lb != end() and not key_comp()(key, lb->first)))
			return lb->second;
		else
			throw std::invalid_argument(errorMsg);
	}

	/* Removes the key (and its corresponding value) from this properties object.
	 * This call does nothing if the specified string is not a key in this properties object.
	 * Same as calling erase(), but returns the value to which the key had been mapped in this properties object,
	 * or an empty string if the key did not have a mapping. */
	string Properties::remove(const string& key)
	{
		const string tmpValue = (*this)[key];
		erase(key);
		return tmpValue;
	}

	/* Removes from this properties object all keys whose corresponding values are empty.
	 * If 'treatWhitespaceAsEmpty' is true, entries with whitespace-only values are treated as empty.
	 * Returns the number of entries removed by this call. */
	unsigned Properties::eraseEmptyEntries(const bool testTrimmed)
	{
		unsigned erasedCount = 0;
		map<string, string>::iterator it = begin();
		while(it != end())
			if(testTrimmed? futil::trim(it->second).empty() : it->second.empty())
				erase(it++), erasedCount++;
			else
				++it;
		return erasedCount;
	}

	/* Tests if the specified string is a key in this properties object. */
	bool Properties::containsKey(const string& key) const
	{
		#if __cplusplus >= 202002L and (not defined(_MSC_VER) or _MSC_VER >= 1921)
			// std::map::contains is available through standard C++ library
			return contains(key);
		#else
			// futil implementation
			map<string, string>::const_iterator lb = lower_bound(key);
			return (lb != end() and not key_comp()(key, lb->first));
		#endif
	}

	/* Reads a property list (key and element pairs) from the given filename in a simple line-oriented format. */
	void Properties::load(const string& filename)
	{
		std::ifstream stream(filename.c_str());
		if(not stream.is_open())
			throw std::runtime_error("File could not be opened: " + filename);

		load(stream);
		stream.close();
	}

	/* Reads a property list (key and element pairs) from the given input stream in a simple line-oriented format.
	 * The specified stream remains open after this method returns. */
	void Properties::load(std::istream& stream)
	{
		string line, key, value;
		while(stream.good())
		{
			getline(stream, line);
			if(extractKeyValueFromLine(line, key, value))
				(*this)[key] = value;
		}
	}

	/* Writes this property list (key and element pairs) in this Properties object to the given filename in a
	 * format suitable for using the load() method. If 'comments' is an empty string, no comment line is written. */
	void Properties::store(const string& filename, const string& comments)
	{
		std::ofstream stream(filename.c_str());
		if(not stream.is_open())
			throw std::runtime_error("File could not be opened: " + filename);

		store(stream, comments);
		stream.close();
	}

	/* Writes this property list (key and element pairs) in this Properties object to the given file stream in a
	 * format suitable for using the load() method. If 'comments' is an empty string, no comment line is written.
	 * The specified stream remains open after this method returns. */
	void Properties::store(std::ofstream& stream, const string& comments)
	{
		if(not stream.is_open())
			throw std::runtime_error("Could not store properties: stream not open");

		if(not comments.empty())
			stream << '#' << comments << '\n';

		for(map<string, string>::iterator it = begin(); it != end(); ++it)
			stream << it->first << "=" << it->second << '\n';
	}

	/* Writes this property list (key and element pairs) in this Properties object to the given filename in a
	 * format suitable for using the load() method, while preserving comments and lines with unchanged values.
	 * This behavior contrasts with the store() method, which truncates an existing file before writing to it,
	 * causing loss of previous comments, spacing and indentation.
	 * If the file contains a key not found in this Properties object, it will be erased accordingly.
	 * This method requires an existing file to work, so use the store() method if file doesn't exist. */
	void Properties::storeUpdate(const string& filename)
	{
		std::ifstream streamIn(filename.c_str());
		if(not streamIn.is_open())
			throw std::runtime_error("File could not be opened: " + filename);

		std::stringstream streamBuffer;
		vector<string> storedKeys;
		string line, key, value;
		for(bool once=true; streamIn.good();)
		{
			// adds line break except if it's the first line
			if(once) once = false;
			else streamBuffer << '\n';

			getline(streamIn, line);
			if(extractKeyValueFromLine(line, key, value))
			{
				if(not futil::contains_element(storedKeys, key)) // check if an entry was already stored for this key
				{
					if(this->containsKey(key))  // check if key is present in the current Properties object
					{
						if(value != (*this)[key])  // check if value differs from the one in the current Properties object
							streamBuffer << key << "=" << (*this)[key];  // store updated value (FIXME fix this to keep original separator instead of replacing it with '=')
						else  // same value
							streamBuffer << line;  // line copied from the original and kept unchanged
					}
					// else then the key is not in the current Properties object, meaning the key is to be removed - so, no action and if will be left out
				}
				// else then the key was already stored, meaning this is a repeated entry and needs to be removed - so, no action and if will be left out
			}
			else  // line is not a valid key-value entry, meaning it should be ignored - so it will be copied from the original and kept unchanged
				streamBuffer << line;
		}
		streamIn.close();

		std::ofstream streamOut(filename.c_str());
		if(not streamOut.is_open())
			throw std::runtime_error("File could not be re-opened: " + filename);

		streamOut << streamBuffer.str();
		streamOut.close();
	}

	/* Searches for the property with the specified key within the specified section in this object and returns it.
	 * If the section or key are not found, an empty string or an optionally provided 'defaultValue' is returned. */
	string SectionedProperties::get(const string& section, const string& key, const string& defaultValue) const
	{
		map<string, Properties>::const_iterator lb = lower_bound(section);
		if((lb != end() and not key_comp()(section, lb->first)))
			return lb->second.get(key, defaultValue);
		else
			return defaultValue;
	}

	/* Searches for the property with the specified key within the specified section in this object and returns it.
	 * Otherwise a std::invalid_argument exception is thrown, optionally with the given 'errorMsg'. */
	string SectionedProperties::retrieve(const string& section, const string& key, const string& errorMsg) const
	{
		map<string, Properties>::const_iterator lb = lower_bound(section);
		if((lb != end() and not key_comp()(section, lb->first)))
			return lb->second.retrieve(key, errorMsg);
		else
			throw std::invalid_argument(errorMsg);
	}

	/* Tests if the specified string is a section in this object. */
	bool SectionedProperties::containsSection(const string& section) const
	{
		#if __cplusplus >= 202002L and (not defined(_MSC_VER) or _MSC_VER >= 1921)
			// std::map::contains is available through standard C++ library
			return contains(section);
		#else
			map<string, Properties>::const_iterator lb = lower_bound(section);
			return (lb != end() and not key_comp()(section, lb->first));
		#endif
	}

	/* Removes the key (and its corresponding value) from the specified section in this object.
	 * This call does nothing if the key or section are not found. */
	string SectionedProperties::remove(const string& section, const string& key)
	{
		map<string, Properties>::iterator lb = lower_bound(section);
		if((lb != end() and not key_comp()(section, lb->first)))
			return lb->second.remove(key);
		else
			return string();
	}

	/* Reads to this object all properties (key/element pairs) in their sections from the given configuration file */
	void SectionedProperties::load(const string& filename)
	{
		std::ifstream stream(filename.c_str());
		if(not stream.is_open())
			throw std::runtime_error("File could not be opened: " + filename);

		string str, currentSectionName;
		std::stringstream streamBuffer;  // buffer to be used by Properties::load()
		while(stream.good())
		{
			getline(stream, str);
			str = trim(str);

			if((starts_with(str, "[") and ends_with(str, "]")) or stream.eof())
			{
				// rewinds the buffer
				streamBuffer.seekg(0);

				// load properties with lines stored in buffer
				(*this)[currentSectionName].load(streamBuffer);

				// if there is more, prepare for next lines
				if(not stream.eof())
				{
					// resets the buffer
					streamBuffer.str(string());
					streamBuffer.clear();

					// set next section
					str = str.substr(str.find('[')+1);
					currentSectionName = str.substr(0, str.rfind(']'));
				}
			}
			else streamBuffer << str << '\n';  // keep line to be read by Properties::load()
		}
		stream.close();
	}

	void SectionedProperties::store(const string& filename, const string& comments)
	{
		std::ofstream stream(filename.c_str());
		if(not stream.is_open())
			throw std::runtime_error("File could not be opened: " + filename);

		// writes "global" section and comments
		(*this)[""].store(stream, comments);

		for(map<string, Properties>::iterator it = begin(); it != end(); ++it)
		{
			if(it->first.empty()) continue;
			stream << "\n[" << it->first << ']' << '\n';
			it->second.store(stream);
		}

		stream.close();
	}
}

#endif /* FUTIL_DISABLE_PROPERTIES_IMPLEMENTATION */
