/*
 * listener.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_LISTENER_HPP_
#define FUTIL_LISTENER_HPP_

#include <algorithm>
#include <vector>
#include <cstdlib>

namespace futil
{
	template<typename ListenerType, typename CallbackType=void(*)()>
	struct ListenerManager
	{
		/// A pointer to a std::vector of Listener's pointers. Initialized with NULL, but instantiated if any Listener is added through addListener().
		std::vector<ListenerType*>* listeners;

		/// (Optional) A function pointer to a callback. Initialized with NULL.
		CallbackType callback;

		ListenerManager() : listeners(NULL), callback(NULL) {}

		/// Adds a listener to this manager's list of listeners. Advised to be used if usage of [] operator is wanted to access listeners.
		void addListener(ListenerType* listener)
		{
			if(listeners == NULL)
				listeners = new std::vector<ListenerType*>(); //instantiate on demand
			if(std::find(listeners->begin(), listeners->end(), listener) == listeners->end())
				listeners->push_back(listener);
		}

		/// Removes an added Listener. If none were added or the given listener were not added, nothing is changed.
		void removeListener(ListenerType* listener)
		{
			if(listeners == NULL) return;
			typename std::vector<ListenerType*>::iterator it = std::find(listeners->begin(), listeners->end(), listener);
			if(it != listeners->end()) //found it
				listeners->erase(it);
		}

		/// Returns the listeners count.
		unsigned size()
		{
			if(listeners == NULL) return 0;
			else return listeners->size();
		}

		/// Used to access or iterate over the listeners.
		ListenerType* operator[](unsigned index)
		{
			if(listeners == NULL) return NULL;
			return listeners->at(index);
		}
	};
}

/** Helper macro to define a basic observer pattern.
* - A Listener class will created with a single pure virtual method named 'callbackName' with the given 'callbackSignature'.
* - A ListenerManager instance will be created with template parameters <Listener, void (*) callbackSignature>, named 'listenerManager'.
*/
#define MakeListenableAs(callbackName, callbackSignature)\
	struct Listener\
	{\
		virtual ~Listener(){}\
		virtual void callbackName callbackSignature = 0;\
	};\
	futil::ListenerManager<Listener, void (*) callbackSignature> listenerManager;

/** Helper macro to define a basic observer pattern.
* - A Listener class will created with a single pure virtual method named 'callbackName' with the given 'callbackSignature'.
* - A ListenerManager instance will be created with template parameters <Listener, void (*) callbackSignature>, named 'listenerManager'.
* - An auxiliary notifyAll method with 'callbackSignature', useful to call all the callbacks with a single statement.
* Note that 'notifyParameters' should match the 'callbackSignature' types.
*/
#define MakeListenableAndNotifyAs(callbackName, callbackSignature, notifyParameters)\
	MakeListenableAs(callbackName, callbackSignature)\
	void notifyAll callbackSignature\
	{\
		if(listenerManager.callback != NULL) listenerManager.callback notifyParameters;\
		for(unsigned i = 0; i < listenerManager.size(); i++) listenerManager[i]->callbackName notifyParameters;\
	}\

#endif /* FUTIL_LISTENER_HPP_ */
