/*
 * string_extra_operators.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_STRING_EXTRA_OPERATORS_IMPLEMENTATION

#include "string_extra_operators.hpp"

#include <cstdio>

using std::string;

#ifndef __MINGW32__
	// regular sprintf
	#define proper_sprintf sprintf
#else
	// needed when using MinGW since it uses MS C runtime, which doesn't support 80 bit floating point numbers and "hh" length
	#define proper_sprintf __mingw_sprintf
#endif

#if __cplusplus >= 201103L
	// standard format
	#define signed_char_format "%hhi"
	#define unsigned_char_format "%hhu"
#else
	// old standard format
	#define signed_char_format "%hi"
	#define unsigned_char_format "%hu"
#endif

#ifdef FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED
	#ifndef __MINGW32__
		// standard format
		#define	long_long_format "%lli"
		#define	long_long_unsigned_format "%llu"
	#else
		// win32 runtime format
		#define long_long_format "%I64i"
		#define long_long_unsigned_format "%I64u"
	#endif
#endif

static char buffer[1024];

string operator + (string a, bool b) { return a + (b? "true" : "false"); }
string operator + (bool a, string b) { return (a? "true" : "false") + b; }

string operator + (string a, signed char b) { proper_sprintf(buffer, signed_char_format, b); return a + buffer; }
string operator + (signed char a, string b) { proper_sprintf(buffer, signed_char_format, a); return buffer + b; }

string operator + (string a, unsigned char b) { proper_sprintf(buffer, unsigned_char_format, b); return a + buffer; }
string operator + (unsigned char a, string b) { proper_sprintf(buffer, unsigned_char_format, a); return buffer + b; }

string operator + (string a, short b) { sprintf(buffer, "%hi", b); return a + buffer; }
string operator + (short a, string b) { sprintf(buffer, "%hi", a); return buffer + b; }

string operator + (string a, unsigned short b) { sprintf(buffer, "%hu", b); return a + buffer; }
string operator + (unsigned short a, string b) { sprintf(buffer, "%hu", a); return buffer + b; }

string operator + (string a, int b) { sprintf(buffer, "%i", b); return a + buffer; }
string operator + (int a, string b) { sprintf(buffer, "%i", a); return buffer + b; }

string operator + (string a, unsigned b) { sprintf(buffer, "%u", b); return a + buffer; }
string operator + (unsigned a, string b) { sprintf(buffer, "%u", a); return buffer + b; }

string operator + (string a, long b) { sprintf(buffer, "%li", b); return a + buffer; }
string operator + (long a, string b) { sprintf(buffer, "%li", a); return buffer + b; }

string operator + (string a, unsigned long b) { sprintf(buffer, "%lu", b); return a + buffer; }
string operator + (unsigned long a, string b) { sprintf(buffer, "%lu", a); return buffer + b; }

string operator + (string a, float b) { sprintf(buffer, "%g", b); return a + buffer; }
string operator + (float a, string b) { sprintf(buffer, "%g", a); return buffer + b; }

string operator + (string a, double b) { sprintf(buffer, "%g", b); return a + buffer; }
string operator + (double a, string b) { sprintf(buffer, "%g", a); return buffer + b; }

string operator + (string a, long double b) { proper_sprintf(buffer, "%Lg", b); return a + buffer; }
string operator + (long double a, string b) { proper_sprintf(buffer, "%Lg", a); return buffer + b; }

#ifdef FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED

string operator + (string a, long long b) { sprintf(buffer, long_long_format, b); return a + buffer; }
string operator + (long long a, string b) { sprintf(buffer, long_long_format, a); return buffer + b; }

string operator + (string a, unsigned long long b) { sprintf(buffer, long_long_unsigned_format, b); return a + buffer; }
string operator + (unsigned long long a, string b) { sprintf(buffer, long_long_unsigned_format, a); return buffer + b; }

#endif /* FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED */

string operator + (string a, void* b) { sprintf(buffer, "%p", b); return a + buffer; }
string operator + (void* a, string b) { sprintf(buffer, "%p", a); return b + buffer; }

#endif /* FUTIL_DISABLE_STRING_EXTRA_OPERATORS_IMPLEMENTATION */
