/*
 * strtox.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STRTOX_H_
#define FUTIL_STRTOX_H_

#ifdef __cplusplus
	#include <cstdlib>
#else
	#include <stdlib.h>
#endif

#ifdef __cplusplus
namespace futil {
extern "C" {
#endif

int strtoi(const char* str, char** endptr, int base);

unsigned strtou(const char* str, char** endptr, int base);

short strtoh(const char* str, char** endptr, int base);

unsigned short strtouh(const char* str, char** endptr, int base);

signed char strtoc(const char* str, char** endptr, int base);

unsigned char strtouc(const char* str, char** endptr, int base);

// define this macro if conflicting with a builtin implementation
#ifndef FUTIL_DISABLE_STRTOF_IMPLEMENTATION

float strtof(const char* str, char** endptr);

#endif /* FUTIL_DISABLE_STRTOF_IMPLEMENTATION */

#ifdef __cplusplus
}}
#endif

#ifdef __cplusplus
	// Alias std::strtoX functions to futil:: as well.
	namespace futil
	{
		// these are always present in std
		using std::strtod;
		using std::strtol;
		using std::strtoul;

		#ifdef FUTIL_DISABLE_STRTOF_IMPLEMENTATION
			using std::strtof;
		#endif
	}

	// Optional aliasing from futil:: to std::
	#ifdef FGEAL_STRTOX_ON_STD
		namespace std
		{
			using futil::strtoi;
			using futil::strtou;
			using futil::strtoh;
			using futil::strtouh;
			using futil::strtoc;
			using futil::strtouc;

			#ifndef FUTIL_DISABLE_STRTOF_IMPLEMENTATION
				using futil::strtof;
			#endif /* FUTIL_DISABLE_STRTOF_IMPLEMENTATION */
		}
	#endif /* FGEAL_STRTOX_ON_STD */

#endif /* __cplusplus */

#endif /* FUTIL_STRTOX_H_ */
