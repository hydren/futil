/*
 * string_actions.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_STRING_ACTIONS_IMPLEMENTATION

#include "string_actions.hpp"

#include <algorithm>
#include <cstdio>

using std::string;

#ifndef __MINGW32__
	// regular sprintf
	#define proper_sprintf sprintf
#else
	// needed when using MinGW since it uses MS C runtime, which doesn't support 80 bit floating point numbers and "hh" length
	#define proper_sprintf __mingw_sprintf
#endif

#if __cplusplus >= 201103L
	// standard format
	#define signed_char_format "%hhi"
	#define unsigned_char_format "%hhu"
#else
	// old standard format
	#define signed_char_format "%hi"
	#define unsigned_char_format "%hu"
#endif

#ifdef FGEAL_TO_STRING_C99_TYPES_ENABLED
	#ifndef __MINGW32__
		// standard format
		#define	long_long_format "%lli"
		#define	long_long_unsigned_format "%llu"
	#else
		// win32 runtime format
		#define long_long_format "%I64i"
		#define long_long_unsigned_format "%I64u"
	#endif
#endif

namespace futil
{
	/* Returns a copy of the string, with leading and trailing whitespace omitted. */
	string& trim(string* str, const char* rippedChars)
	{
		str->erase(str->find_last_not_of(rippedChars) + 1); //trailing
		str->erase(0, str->find_first_not_of(rippedChars)); //leading
		return *str;
	}

	string& ltrim(string* str, const char* rippedChars)
	{
		str->erase(0, str->find_first_not_of(rippedChars)); //leading
		return *str;
	}

	string& rtrim(string* str, const char* rippedChars)
	{
		str->erase(str->find_last_not_of(rippedChars) + 1); //trailing
		return *str;
	}

	/** Returns a copy of the string, with leading and trailing whitespace omitted. */
	string trim(const string& str, const char* rippedChars)
	{
		string str2 = str;
		return trim(&str2);
	}

	string ltrim(const string& str, const char* rippedChars)
	{
		string str2 = str;
		return ltrim(&str2);
	}

	string rtrim(const string& str, const char* rippedChars)
	{
		string str2 = str;
		return rtrim(&str2);
	}

	/* Returns true if the given string str ends with the given string ending */
	bool ends_with (const string& str, const string& ending)
	{
		if (str.length() >= ending.length())
			return (0 == str.compare (str.length() - ending.length(), ending.length(), ending));
		else
			return false;
	}

	/* Returns true if the given string str starts with the given string beginning */
	bool starts_with (string const& str, string const& beginning)
	{
		if(str.length() >= beginning.length())
			return (0 == str.compare (0, beginning.length(), beginning));
		else
			return false;
	}

	string& replace_all(string* const s, const string& from, const string& to)
	{
		string& str = *s;
		size_t start_pos = 0;
		while((start_pos = str.find(from, start_pos)) != string::npos) {
			str.replace(start_pos, from.length(), to);
			start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
		}
		return str;
	}

	string replace_all(const string& str, const string& from, const string& to)
	{
		string copy = string(str);
		return replace_all(&copy, from, to);
	}

	string& to_lower(string* str)
	{
		string& s = *str;
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
		return s;
	}

	string to_lower(const string& str)
	{
		string copy = string(str);
		return to_lower(&copy);
	}

	string& to_upper(string* str)
	{
		string& s = *str;
		std::transform(s.begin(), s.end(), s.begin(), ::toupper);
		return s;
	}

	string to_upper(const string& str)
	{
		string copy = string(str);
		return to_upper(&copy);
	}

	/* Returns true if the given string contains the given token. */
	bool contains(const string& str, const string& token)
	{
		return (str.find(token) != string::npos);
	}

	static char buffer[1024];

	string to_string(int value)
	{
		sprintf(buffer, "%i", value);
		return buffer;
	}

	string to_string(long value)
	{
		sprintf(buffer, "%li", value);
		return buffer;
	}

	string to_string(unsigned value)
	{
		sprintf(buffer, "%u", value);
		return buffer;
	}

	string to_string(unsigned long value)
	{
		sprintf(buffer, "%lu", value);
		return buffer;
	}

	string to_string(float value)
	{
		sprintf(buffer, "%g", value);
		return buffer;
	}

	string to_string(double value)
	{
		sprintf(buffer, "%g", value);
		return buffer;
	}

	string to_string(long double value)
	{
		proper_sprintf(buffer, "%Lg", value);
		return buffer;
	}

	string to_string(void* ptr)
	{
		sprintf(buffer, "%p", ptr);
		return buffer;
	}

	string to_string(bool value)
	{
		return value? "true" : "false";
	}

	string to_string(short value)
	{
		sprintf(buffer, "%hi", value);
		return buffer;
	}

	string to_string(unsigned short value)
	{
		sprintf(buffer, "%hu", value);
		return buffer;
	}

	string to_string(signed char value)
	{
		proper_sprintf(buffer, signed_char_format, value);
		return buffer;
	}

	string to_string(unsigned char value)
	{
		proper_sprintf(buffer, unsigned_char_format, value);
		return buffer;
	}

	// C99 / C++11 excluse types
	#ifdef FGEAL_TO_STRING_C99_TYPES_ENABLED

	string to_string(long long value)
	{
		sprintf(buffer, long_long_format, value);
		return buffer;
	}

	string to_string(unsigned long long value)
	{
		sprintf(buffer, long_long_unsigned_format, value);
		return buffer;
	}

	#endif /* FGEAL_TO_STRING_C99_TYPES_ENABLED */
}

#endif /* FUTIL_DISABLE_STRING_ACTIONS_IMPLEMENTATION */
