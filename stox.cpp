/*
 * stox.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FGEAL_DISABLE_STOX_IMPLEMENTATION

#include "stox.hpp"

#include "strtox.h"

#include <cstdlib>
#include <cerrno>
#include <climits>
#include <cfloat>
#include <stdexcept>

#define null NULL

using std::size_t;
using std::string;

#define STRTOX_BASE_PASSED , base
#define STRTOX_NO_BASE

#define STRTOX(T, T_FUNC, STRTOX_BASE)\
	char* end;\
	errno = 0;\
	const T t = T_FUNC(str.c_str(), &end STRTOX_BASE);\
	if(errno == ERANGE)\
		throw std::out_of_range(str);\
	\
	if (*str.c_str() == '\0' or *end != '\0')\
		throw std::invalid_argument(str);\
	\
	if(pos != null)\
		*pos = end - str.c_str();\
	\
	return t;

namespace futil
{
	#ifdef FGEAL_STOX_CPP11_BACKPORT_ENABLED

	int stoi(const string& str, std::size_t* pos, int base)
	{
		STRTOX(int, futil::strtoi, STRTOX_BASE_PASSED)
	}

	long stol(const string& str, std::size_t* pos, int base)
	{
		STRTOX(long, futil::strtol, STRTOX_BASE_PASSED)
	}

	unsigned long stoul( const string& str, std::size_t* pos, int base)
	{
		STRTOX(unsigned long, futil::strtoul, STRTOX_BASE_PASSED)
	}

	float stof(const string& str, std::size_t* pos )
	{
		STRTOX(float, futil::strtof, STRTOX_NO_BASE)
	}

	double stod(const string& str, std::size_t* pos )
	{
		STRTOX(double, futil::strtod, STRTOX_NO_BASE)
	}

	#endif /* FGEAL_STOX_CPP11_BACKPORT_ENABLED */

	#if __cplusplus < 201103L

	unsigned stou(const string& str, std::size_t* pos, int base)
	{
		STRTOX(unsigned, futil::strtou, STRTOX_BASE_PASSED)
	}

	short stoh(const string& str, std::size_t* pos, int base)
	{
		STRTOX(short, futil::strtoh, STRTOX_BASE_PASSED)
	}

	unsigned short stouh(const string& str, std::size_t* pos, int base)
	{
		STRTOX(unsigned short, futil::strtouh, STRTOX_BASE_PASSED)
	}

	signed char stoc(const string& str, std::size_t* pos, int base)
	{
		STRTOX(signed char, futil::strtoc, STRTOX_BASE_PASSED)
	}

	unsigned char stouc(const string& str, std::size_t* pos, int base)
	{
		STRTOX(unsigned char, futil::strtouc, STRTOX_BASE_PASSED)
	}

	#else
		template <typename T, T T_MIN, T T_MAX>
		static inline T stol_ranged(const string& str, size_t* pos, int base)
		{
			const long t = std::stol(str, pos, base);
			if(t > T_MAX or t < T_MIN)
				throw std::out_of_range(str);
			return static_cast<T>(t);
		}

		template <typename T, T T_MAX>
		static inline T stoul_ranged(const string& str, size_t* pos, int base)
		{
			const unsigned long t = std::stoul(str, pos, base);
			if(t > T_MAX)
				throw std::out_of_range(str);
			return static_cast<T>(t);
		}

		unsigned stou(const string& str, std::size_t* pos, int base)
		{
			return stoul_ranged<unsigned, UINT_MAX>(str, pos, base);
		}

		short stoh(const string& str, std::size_t* pos, int base)
		{
			return stol_ranged<short, SHRT_MIN, SHRT_MAX>(str, pos, base);
		}

		unsigned short stouh(const string& str, std::size_t* pos, int base)
		{
			return stoul_ranged<unsigned short, USHRT_MAX>(str, pos, base);
		}

		signed char stoc(const string& str, std::size_t* pos, int base)
		{
			return stol_ranged<signed char, SCHAR_MIN, SCHAR_MAX>(str, pos, base);
		}

		unsigned char stouc(const string& str, std::size_t* pos, int base)
		{
			return stoul_ranged<unsigned char, UCHAR_MAX>(str, pos, base);
		}

	#endif /* __cplusplus < 201103L */
}

#endif /* FGEAL_DISABLE_STOX_IMPLEMENTATION */
