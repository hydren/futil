/*
 * string_parse.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_STRING_PARSE_IMPLEMENTATION

#include "string_parse.hpp"
#include "stox.hpp"

#define null NULL

using std::string;

namespace futil
{
	template <>
	int parse(const std::string& str, unsigned base)
	{
		return futil::stoi(str, null, base);
	}

	template <>
	short parse(const std::string& str, unsigned base)
	{
		return futil::stoh(str, null, base);
	}

	template <>
	long parse(const std::string& str, unsigned base)
	{
		return futil::stol(str, null, base);
	}

	template <>
	unsigned parse(const std::string& str, unsigned base)
	{
		return futil::stou(str, null, base);
	}

	template <>
	unsigned short parse(const std::string& str, unsigned base)
	{
		return futil::stouh(str, null, base);
	}

	template <>
	unsigned long parse(const std::string& str, unsigned base)
	{
		return futil::stoul(str, null, base);
	}

	template <>
	signed char parse(const std::string& str, unsigned base)
	{
		return futil::stoc(str, null, base);
	}

	template <>
	unsigned char parse(const std::string& str, unsigned base)
	{
		return futil::stouc(str, null, base);
	}

	template <>
	float parse(const std::string& str, unsigned base)
	{
		return futil::stof(str, null);
	}

	template <>
	double parse(const std::string& str, unsigned base)
	{
		return futil::stod(str, null);
	}

	template <>
	bool parse(const std::string& str, unsigned base)
	{
		if(str == "true")  return true;
		if(str == "false") return false;
		throw std::invalid_argument(str);
	}

	#ifdef FGEAL_STRING_PARSE_C99_TYPES_ENABLED

	template <>
	long double parse(const std::string& str, unsigned base)
	{
		return futil::stold(str, null);
	}

	template <>
	long long parse(const std::string& str, unsigned base)
	{
		return futil::stoll(str, null);
	}

	template <>
	unsigned long long parse(const std::string& str, unsigned base)
	{
		return futil::stoull(str, null);
	}

	#endif /* FGEAL_STRING_PARSE_C99_TYPES_ENABLED */
}

#endif /* FUTIL_DISABLE_STRING_PARSE_IMPLEMENTATION */
