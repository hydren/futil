/*
 * random.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_RANDOM_H_
#define FUTIL_RANDOM_H_

#ifdef __cplusplus
namespace futil {
extern "C" {
#endif

/** Returns a random integer between 'min' and 'max' (in the range [min, max]) */
int random_between(int min, int max);

/** Returns a random decimal between 'min' and 'max' (in the range [min, max]) */
double random_between_decimal(double min, double max);

/** Returns a random decimal between 'min' and 'max' (in the range [min, max])
 *  This version has a normal/gaussian distribuition (as opposed to a uniform distribuition) */
double random_between_gauss(double min, double max);

#ifdef __cplusplus
}}
#endif

#endif /* FUTIL_RANDOM_H_ */
