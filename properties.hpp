/*
 * properties.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_PROPERTIES_HPP_
#define FUTIL_PROPERTIES_HPP_
#include <ciso646>

#include <cstdlib>
#include <string>
#include <map>
#include <fstream>

#include "language.hpp"

namespace futil
{
	/** A structure that holds data read from a .properties file (similar to Java's Properties class).
	 *  Basically, the data consists of a list of key and element pairs, referred as "properties".
	 *  This implementation stores properties as a std::map<string, string>, providing methods for access/modification.
	 *  For example, a reference to a property associated with some key is available through the operator[] (as with
	 *  std::map::operator[], if the key is not found, it is created with an empty value and then returned).
	 *  Unlike in Java, it does not possess a "defaults" object, neither support multi-line values (yet). */
	struct Properties extends std::map<std::string, std::string>
	{
		// TODO: add support for multi-line values, since a .properties may have them

		/** Maps the specified key to the specified value in this properties object.
		 *  The value can be retrieved by calling the get method with a key that is equal to the original key.
		 *  Same as 'properties[key] = value', but returns the previous value of the specified key in this
		 *  property list, or an empty string if it did not have one. */
		std::string put(const std::string& key, const std::string& value);

		/** Searches for the property with the specified key in this properties object and returns it.
		 *  If the key is not found, an empty string or an optionally provided 'defaultValue' is returned. */
		std::string get(const std::string& key, const std::string& defaultValue=std::string()) const;

		/** Searches for the property with the specified key in this properties object and returns it if found.
		 *  Otherwise a std::invalid_argument exception is thrown, optionally with the given 'errorMsg'. */
		std::string retrieve(const std::string& key, const std::string& errorMsg=std::string()) const;

		/** Removes the key (and its corresponding value) from this properties object.
		 *  This call does nothing if the specified string is not a key in this properties object.
		 *  Same as calling erase(), but returns the value to which the key had been mapped in this properties object,
		 *  or an empty string if the key did not have a mapping. */
		std::string remove(const std::string& key);

		/** Removes from this properties object all keys whose corresponding values are empty.
		 *  If 'treatWhitespaceAsEmpty' is true, entries with whitespace-only values are treated as empty.
		 *  Returns the number of entries removed by this call. */
		unsigned eraseEmptyEntries(const bool treatWhitespaceAsEmpty=false);

		/** Tests if the specified string is a key in this properties object. */
		bool containsKey(const std::string& key) const;

		/** Reads a property list (key and element pairs) from the given filename in a simple line-oriented format. */
		void load(const std::string& filename);

		/** Reads a property list (key and element pairs) from the given input stream in a simple line-oriented format.
		 *  The specified stream remains open after this method returns. */
		void load(std::istream& inputStream);

		/** Writes this property list (key and element pairs) in this Properties object to the given filename in a
		 *  format suitable for using the load() method. If 'comments' is an empty string, no comment line is written.*/
		void store(const std::string& filename, const std::string& comments=std::string());

		/** Writes this property list (key and element pairs) in this Properties object to the given file stream in a
		 *  format suitable for using the load() method. If 'comments' is an empty string, no comment line is written.
		 *  The specified stream remains open after this method returns. */
		void store(std::ofstream& outputFileStream, const std::string& comments=std::string());

		/** Writes this property list (key and element pairs) in this Properties object to the given filename in a
		 *  format suitable for using the load() method, while preserving comments and lines with unchanged values.
		 *  This behavior contrasts with the store() method, which truncates an existing file before writing to it,
		 *  causing loss of previous comments, spacing and indentation.
		 *  If the file contains a key not found in this Properties object, it will be erased accordingly.
		 *  This method requires an existing file to work, so use the store() method if file doesn't exist. */
		void storeUpdate(const std::string& filename);

		/** Behaves the same way as get(), but the key's corresponding value is converted to T before returning it.
		 *  Conversion is done from std::string to T, using the function specified in the template parameters. */
		template <typename T, T (*parseFunction) (const std::string&)> inline
		T getParsed(const std::string& key, T defaultValue=T()) const
		{
			return containsKey(key)? parseFunction(get(key)) : defaultValue;
		}

		/** Behaves the same way as getParsed(), but uses a conversion function that expects a const char* instead of a
		 *  std::string. */
		template <typename T, T (*parseFunction) (const char*)> inline
		T getParsedCStr(const std::string& key, T defaultValue=T()) const
		{
			return containsKey(key)? parseFunction(get(key).c_str()) : defaultValue;
		}

		/** Returns either the value from this properties instance, converted from string to integer using 'atoi' or
		 *  returns a default value (optionally, user-provided) if the key is missing. */
		inline int getInteger(const std::string& key, int defaultValue=0) const
		{
			return getParsedCStr<int, atoi>(key, defaultValue);
		}

		/** Returns either the value from this properties instance, converted from string to double using 'atof' or
		 *  returns a default value (optionally, user-provided) if the key is missing. */
		inline double getDouble(const std::string& key, float defaultValue=0) const
		{
			return getParsedCStr<double, atof>(key, defaultValue);
		}

		/** Behaves the same way as retrieve(), but the key's corresponding value is converted to T before returning it.
		 *  Conversion is done from std::string to T, using the function specified in the template parameters. */
		template <typename T, T (*parseFunction) (const std::string&)> inline
		T retrieveParsed(const std::string& key, const std::string failMessage=std::string()) const
		{
			return parseFunction(retrieve(key, failMessage));
		}

		/** Behaves the same way as retrieveParsed(), but uses a conversion function that expects a const char* instead
		 *  of a std::string. */
		template <typename T, T (*parseFunction) (const char*)> inline
		T retrieveParsedCStr(const std::string& key, const std::string failMessage=std::string()) const
		{
			return parseFunction(retrieve(key, failMessage).c_str());
		}

		/** Returns either the value from this properties instance, converted from string to integer using the 'atoi'
		 *  or throws a std::invalid_argument exception (optionally with a custom message) if the key is missing. */
		inline int retrieveInteger(const std::string& key, const std::string failMessage=std::string()) const
		{
			return retrieveParsedCStr<int, atoi>(retrieve(key, failMessage));
		}

		/** Returns either the value from this properties instance, converted from string to double using the 'atof'
		 *  or throws a std::invalid_argument exception (optionally with a custom message) if the key is missing. */
		inline double retrieveDouble(const std::string& key, const std::string failMessage=std::string()) const
		{
			return retrieveParsedCStr<double, atof>(retrieve(key, failMessage));
		}

		/** Behaves the same way as getParsed(), but when the key's corresponding value is equal to "default",
		 *  instead of actually parsing the key's value, it returns the default value. */
		template <typename T, T (*parseFunction) (const std::string&)> inline
		T getParsedAllowDefault(const std::string& key, T defaultValue=T()) const
		{
			return containsKey(key) and get(key) != "default"? parseFunction(get(key)) : defaultValue;
		}

		/** Behaves the same way as getParsedCStr(), but when the key's corresponding value is equal to "default",
		 *  instead of actually parsing the key's value, it returns the default value. */
		template <typename T, T (*parseFunction) (const char*)> inline
		T getParsedCStrAllowDefault(const std::string& key, T defaultValue=T()) const
		{
			return containsKey(key) and get(key) != "default"? parseFunction(get(key).c_str()) : defaultValue;
		}

		/** Behaves the same way as getInteger(), but when the key's corresponding value is equal to "default",
		 *  it returns the default value (instead of actually parsing the key's value, which would result in 0). */
		inline int getIntegerAllowDefault(const std::string& key, int defaultValue=0) const
		{
			return getParsedCStrAllowDefault<int, atoi>(key, defaultValue);
		}

		/** Behaves the same way as getDouble(), but when the key's corresponding value is equal to "default",
		 *  it returns the default value (instead of actually parsing the key's value, which would result in 0). */
		inline double getDoubleAllowDefault(const std::string& key, double defaultValue=0) const
		{
			return getParsedCStrAllowDefault<double, atof>(key, defaultValue);
		}

		/** Behaves the same as getParsedAllowDefault(), but when the key's corresponding value is equal to "default",
		 *  instead of returning the default value, it returns the given optional value (the default value is returned
		 *  only when the key is missing). */
		template <typename T, T (*parseFunction) (const std::string&)> inline
		T getParsedAllowOptionedDefault(const std::string& key, T optionalValue, T defaultValue=T()) const
		{
			return containsKey(key)? (get(key) == "default"? optionalValue : parseFunction(get(key))) : defaultValue;
		}

		/** Behaves the same as getParsedCStrAllowDefault(), but when the key's corresponding value is equal to "default",
		 *  instead of returning the default value, it returns the given optional value (the default value is returned
		 *  only when the key is missing). */
		template <typename T, T (*parseFunction) (const char*)> inline
		T getParsedCStrAllowOptionedDefault(const std::string& key, T optionalValue, T defaultValue=T()) const
		{
			return containsKey(key)? (get(key) == "default"? optionalValue : parseFunction(get(key).c_str())) : defaultValue;
		}
	};

	/** A structure that holds data read from typical configuration files (such as .ini, .conf or .cfg) which can be
	 *  treated as a set of properties, grouped by sections, hence the name. To access properties associated with a
	 *  specific section in this object, the operator[] is used, with the section name as argument, which then returns
	 *  a reference to the corresponding properties object (as with std::map::operator[], if a section is not found,
	 *  it is created with an empty properties object and then returned). "Global" properties - that is - properties
	 *  that are declared before any section is declared, and thus, don't belong to any section - are allowed and
	 *  accessible in methods by using an empty string ("") as section name. */
	struct SectionedProperties extends std::map<std::string, Properties>
	{
		// TODO add non-destructive write and other utility methods

		/** Maps the specified key to the specified value within the specified section in this object.
		 *  Same as 'object[section][key] = value', but returns the previous value of the specified key within the given
		 *  section, or an empty string if it did not have one (if the specified section doesn't exist, it will be created). */
		inline std::string put(const std::string& section, const std::string& key, const std::string& value)
		{ return (*this)[section].put(key, value); }

		/** Searches for the property with the specified key within the specified section in this object and returns it.
		 *  If the section or key are not found, an empty string or an optionally provided 'defaultValue' is returned. */
		std::string get(const std::string& section, const std::string& key, const std::string& defaultValue=std::string()) const;

		/** Searches for the property with the specified key within the specified section in this object and returns it.
		 *  Otherwise a std::invalid_argument exception is thrown, optionally with the given 'errorMsg'. */
		std::string retrieve(const std::string& section, const std::string& key, const std::string& errorMsg=std::string()) const;

		/** Tests if the specified string is a section in this object. */
		bool containsSection(const std::string& section) const;

		/** Tests if the specified key is a property in the specified section of this object. */
		inline bool containsKey(const std::string& section, const std::string& key)
		{ return containsSection(section) and (*this)[section].containsKey(key); }

		/** Removes the key (and its corresponding value) from the specified section in this object.
		 *  This call does nothing if the key or section are not found. */
		std::string remove(const std::string& section, const std::string& key);

		/** Reads to this object all properties (key/element pairs) in their sections from the given configuration file */
		void load(const std::string& filename);

		/** Writes all properties (key/element pairs) in their sections from this object to the given filename in a
		 *  format suitable for using the load() method. If 'comments' is an empty string, no comment line is written.*/
		void store(const std::string& filename, const std::string& comments=std::string());
	};
}

#endif /* FUTIL_PROPERTIES_HPP_ */
