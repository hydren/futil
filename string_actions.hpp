/*
 * string_actions.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STRING_ACTIONS_HPP_
#define FUTIL_STRING_ACTIONS_HPP_

#include <string>

#if __cplusplus >= 201103L
	#define FGEAL_TO_STRING_C99_TYPES_ENABLED
#endif

namespace futil
{
	/** Returns a copy of the std::string, with leading and trailing whitespace omitted. */
	std::string& trim(std::string* str, const char* rippedChars=" \t\n\r\f\v");

	std::string& ltrim(std::string* str, const char* rippedChars=" \t\n\r\f\v");

	std::string& rtrim(std::string* str, const char* rippedChars=" \t\n\r\f\v");

	/** Returns a copy of the std::string, with leading and trailing whitespace omitted. */
	std::string trim(const std::string& str, const char* rippedChars=" \t\n\r\f\v");

	std::string ltrim(const std::string& str, const char* rippedChars=" \t\n\r\f\v");

	std::string rtrim(const std::string& str, const char* rippedChars=" \t\n\r\f\v");

	/** Returns true if the given std::string str ends with the given std::string postStr */
	bool ends_with (const std::string& str, const std::string& postStr);

	/** Returns true if the given std::string str starts with the given std::string preStr */
	bool starts_with (std::string const& str, std::string const& preStr);

	std::string& replace_all(std::string* const s, const std::string& from, const std::string& to);

	std::string replace_all(const std::string& str, const std::string& from, const std::string& to);

	std::string& to_lower(std::string* str);

	std::string to_lower(const std::string& str);

	std::string& to_upper(std::string* str);

	std::string to_upper(const std::string& str);

	/** Returns true if the given string contains the given token. */
	bool contains(const std::string& str, const std::string& token);

	// similar to C++11's to_string
	std::string to_string(int value);
	std::string to_string(long value);
	std::string to_string(unsigned value);
	std::string to_string(unsigned long value);
	std::string to_string(float value);
	std::string to_string(double value);
	std::string to_string(long double value);

	// as of 2017, unprecedent
	std::string to_string(void* ptr);
	std::string to_string(bool boolean);
	std::string to_string(short sint);
	std::string to_string(unsigned short usint);
	std::string to_string(signed char schar);
	std::string to_string(unsigned char schar);

	// C99 / C++11 excluse types
	#ifdef FGEAL_TO_STRING_C99_TYPES_ENABLED

	std::string to_string(long long value);
	std::string to_string(unsigned long long value);

	#endif /* FGEAL_TO_STRING_C99_TYPES_ENABLED */
}

// add to_string to std namespace when not C++11
#if defined(__cplusplus) && __cplusplus < 201103L && !FGEAL_TO_STRING_ON_STD
	namespace std
	{
		using futil::to_string;
	}
#endif

// template stringify functions (useful to be used as funtion pointer callbacks)
namespace futil
{
	template <typename StringCastableType>
	std::string stringify_by_cast(StringCastableType value)
	{
		return (std::string) value;
	}

	template <typename StringCastableType>
	std::string stringify_by_cast_const(const StringCastableType value)
	{
		return (std::string) value;
	}

	template <typename StringCastableType>
	std::string stringify_by_cast_ptr(StringCastableType* const value)
	{
		return (std::string) *value;
	}

	template <typename StringCastableType>
	std::string stringify_by_cast_const_ptr(const StringCastableType* const value)
	{
		return (std::string) *value;
	}

	template <typename StringCastableType>
	std::string stringify_by_cast_ref(StringCastableType& value)
	{
		return (std::string) value;
	}

	template <typename StringCastableType>
	std::string stringify_by_cast_const_ref(const StringCastableType& value)
	{
		return (std::string) value;
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)()>
	std::string stringify_by_method(StringableType value)
	{
		return (value.*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method(StringableType value)
	{
		return (value.*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method_const(const StringableType value)
	{
		return (value.*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)()>
	std::string stringify_by_method_ptr(StringableType* const value)
	{
		return (value->*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)()>
	std::string stringify_by_method_ref(StringableType& value)
	{
		return (value.*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method_ptr(StringableType* const value)
	{
		return (value->*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method_ref(StringableType& value)
	{
		return (value.*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method_const_ptr(const StringableType* const value)
	{
		return (value->*stringifyMethod)();
	}

	template <typename StringableType, std::string(StringableType::*stringifyMethod)() const>
	std::string stringify_by_const_method_const_ref(const StringableType& value)
	{
		return (value.*stringifyMethod)();
	}
}

#endif /* FUTIL_STRING_ACTIONS_HPP_ */
