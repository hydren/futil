/*
 * stox.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STOX_HPP_
#define FUTIL_STOX_HPP_
#include <ciso646>

#include <string>

#if __cplusplus < 201103L and !defined(FGEAL_STOX_DISABLE_CPP11_BACKPORT)
	#ifndef FGEAL_STOX_CPP11_BACKPORT_ENABLED
		#define FGEAL_STOX_CPP11_BACKPORT_ENABLED
	#endif
#endif

namespace futil
{
	#if defined(FGEAL_STOX_CPP11_BACKPORT_ENABLED)
		// C++98 implementation of C++11 stoX functions
		int stoi(const std::string& str, std::size_t* pos=0, int base=10);
		long stol(const std::string& str, std::size_t* pos=0, int base=10);
		unsigned long stoul( const std::string& str, std::size_t* pos=0, int base=10);
		float stof(const std::string& str, std::size_t* pos=0 );
		double stod(const std::string& str, std::size_t* pos=0 );

	#elif __cplusplus >= 201103L
		using std::stoi;
		using std::stol;
		using std::stoul;
		using std::stof;
		using std::stod;

		// these can only be implemented cleanly with C++11, but will always be available when C++11
		using std::stoll;
		using std::stoull;
		using std::stold;
	#endif

	// custom stoX functions
	unsigned stou(const std::string& str, std::size_t* pos=0, int base=10);

	short stoh(const std::string& str, std::size_t* pos=0, int base=10);
	unsigned short stouh(const std::string& str, std::size_t* pos=0, int base=10);

	signed char stoc(const std::string& str, std::size_t* pos=0, int base=10);
	unsigned char stouc(const std::string& str, std::size_t* pos=0, int base=10);
}

#ifdef FGEAL_STOX_ON_STD
namespace std
{
	#if defined(FGEAL_STOX_CPP11_BACKPORT_ENABLED) && !defined(FGEAL_STOX_CPP11_BACKPORT_NOT_ON_STD)
		using futil::stoi;
		using futil::stol;
		using futil::stoul;
		using futil::stof;
		using futil::stod;
	#endif

	using futil::stou;
	using futil::stoh;
	using futil::stouh;
	using futil::stoc;
	using futil::stouc;
}
#endif

#endif /* FUTIL_STOX_HPP_ */
