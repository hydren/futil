/*
 * round.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_ROUND_H_
#define FUTIL_ROUND_H_

#ifdef __cplusplus
	#if __cplusplus >= 201103L and (not defined(_MSC_VER) or _MSC_VER >= 1800)
		// round is available through standard library, in std namespace
		#define FUTIL_ROUND_FROM_STD
	#elif __cplusplus < 201103L and defined(__GNUG__)
		//round is available through GCC extension, in global namespace
		#define FUTIL_ROUND_FROM_GCC
	#else
		//in this case, round is not available by C++ library, but is supplied by futil
		#define FUTIL_ROUND_ENABLED
	#endif
#else
	#if (defined(_MSC_VER) && _MSC_VER < 1800) || (__STDC_VERSION__ < 199901L && !defined(__GNUG__))
		//in this case, round is not available by C library, but is supplied by futil
		#define FUTIL_ROUND_ENABLED
	#endif
#endif /* __cplusplus */

#if defined(FUTIL_ROUND_ENABLED) && !defined(FUTIL_DISABLE_RANDOM_IMPLEMENTATION)

#ifdef __cplusplus
namespace futil {
extern "C" {
#endif

/** Computes the nearest integer value to x (in floating-point format), rounding halfway cases away from zero. If c++11 is enabled, the <cmath> version is used instead. */
double round(double x);

#ifdef __cplusplus
}}
#endif

#else  //round is available through library
	#ifdef __cplusplus
		// if getting "undeclared identifier" errors for math constants while compiling with MSVC, check if _USE_MATH_DEFINES is defined globally in the compiler settings
		#include <cmath>

		// alias futil::round from std::round
		namespace futil
		{
			#ifdef FUTIL_ROUND_FROM_STD
				using std::round;
			#elif defined(FUTIL_ROUND_FROM_GCC)
				using ::round;
			#endif
		}
	#else
		#ifndef _USE_MATH_DEFINES
			#define _USE_MATH_DEFINES
		#endif  /* _USE_MATH_DEFINES */
		#include <math.h>
	#endif
#endif /* FUTIL_ROUND_ENABLED */

#endif /* FUTIL_ROUND_H_ */
