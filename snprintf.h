/*
 * snprintf.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_SNPRINTF_H_
#define FUTIL_SNPRINTF_H_

#ifndef __cplusplus
	#include <stdio.h>
#else
	#include <cstdio>
#endif

#if defined(_MSC_VER) && _MSC_VER < 1900
	//in this case, snprintf and vsnprintf are not available by C++ library, but can supplied by futil
	#define FUTIL_SNPRINTF_ENABLED
#endif

#ifdef FUTIL_SNPRINTF_ENABLED
	#ifdef __cplusplus
		#include <cstdarg>
		namespace futil {
		extern "C" {
	#else
		#include <stdarg.h>
	#endif

	int c99_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap);
	int c99_snprintf(char *outBuf, size_t size, const char *format, ...);

	#ifdef __cplusplus
	}}
	#endif

	#if !defined(vsnprintf) && !defined(snprintf)
		#define vsnprintf c99_vsnprintf
		#define snprintf c99_snprintf
	#endif

	// Optional aliasing from futil:: to std::
	#if defined(__cplusplus) && defined(FGEAL_SNPRINTF_ON_STD)
		namespace std
		{
			using futil::vsnprintf;
			using futil::snprintf;
		}
	#endif
#else
	#ifdef __cplusplus
		// Aliasing from std:: to futil::
		namespace futil
		{
			#if __cplusplus < 201103L
				using ::vsnprintf;
				using ::snprintf;
			#else
				using std::vsnprintf;
				using std::snprintf;
			#endif
		}
	#endif
#endif /* FUTIL_SNPRINTF_ENABLED */

#endif /* FUTIL_SNPRINTF_H_ */
