/*
 * string_parse.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STRING_PARSE_HPP_
#define FUTIL_STRING_PARSE_HPP_
#include <ciso646>

#include <string>
#include <stdexcept>
#include <sstream>

#if __cplusplus >= 201103L
	#define FGEAL_STRING_PARSE_C99_TYPES_ENABLED
#endif

namespace futil
{
	/** Returns the value represented by the given string, as the specified type (template parameter).
	 *  For non-number types, the 'base' argument is ignored.
	 *  For number types, the 'base' argument specifies the numeric base/radix of the number on the string.
	 *  If 'base' is zero, decimal base is expected unless a specialized version of this template is used, which tries out all bases from the 2-36 range.
	 *  Throws std::invalid_argument if it fails or std::out_of_range if the converted value would fall out of the range of the result type. */
	template <typename T>
	T parse(const std::string& str, unsigned base=0)
	{
		std::stringstream stream(str);
		T value; char dummy;
		if(base ==  8) stream << std::oct;
		if(base == 16) stream << std::hex;
		stream >> value;
		if(stream.fail() or stream.get(dummy))
			throw std::invalid_argument(std::string("Failed to convert ") + str);
		return value;
	}

	/** Returns true if the given string can be parsed to the specified type (template parameter).
	 *  The rules for the 'base' argument are the same of futil::parse(). */
	template <typename T>
	bool parseable(const std::string& str, unsigned base=0)
	{
		try
		{
			parse<T>(str, base);
			return true;
		}
		catch (const std::invalid_argument&) { return false; }
		catch (const std::out_of_range&) { return false; }
	}

	// Specialized versions which doesn't use string streams

	template <>
	int parse(const std::string& str, unsigned base);

	template <>
	short parse(const std::string& str, unsigned base);

	template <>
	long parse(const std::string& str, unsigned base);

	template <>
	unsigned parse(const std::string& str, unsigned base);

	template <>
	unsigned short parse(const std::string& str, unsigned base);

	template <>
	unsigned long parse(const std::string& str, unsigned base);

	template <>
	signed char parse(const std::string& str, unsigned base);

	template <>
	unsigned char parse(const std::string& str, unsigned base);

	template <>
	float parse(const std::string& str, unsigned base);

	template <>
	double parse(const std::string& str, unsigned base);

	template <>
	bool parse(const std::string& str, unsigned base);

	#ifdef FGEAL_STRING_PARSE_C99_TYPES_ENABLED

	template <>
	long double parse(const std::string& str, unsigned base);

	template <>
	long long parse(const std::string& str, unsigned base);

	template <>
	unsigned long long parse(const std::string& str, unsigned base);

	#endif /* FGEAL_STRING_PARSE_C99_TYPES_ENABLED */
}

#endif /* FUTIL_STRING_PARSE_HPP_ */
