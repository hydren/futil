/*
 * random.c
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_RANDOM_IMPLEMENTATION

#include "random.h"

#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES
#endif  /* _USE_MATH_DEFINES */
#include <math.h>
#include <stdlib.h>

int random_between(int min, int max)
{
	int x;
	if (max == min)
		return min;

	if(max < min) //swap
	{
		const int tmp = max;
		max = min;
		min = tmp;
	}

	do x = rand();
	while ((max-min) < RAND_MAX && x >= RAND_MAX - (RAND_MAX % (max-min)));
	return min + x % (max-min);
}

double random_between_decimal(double min, double max)
{
	double f;
	if (max == min)
		return min;

	if(max < min) //swap
	{
		const double tmp = max;
		max = min;
		min = tmp;
	}

    f = ((double) rand()) / (double) RAND_MAX;
    return min + f * (max - min);
}

/** Normal/Gaussian distribuition version. */
double random_between_gauss(double min, double max)
{
	double mean, stdDev, u, v, s;
	if (max == min)
		return max;

	if(max < min) //swap
	{
		const double tmp = max;
		max = min;
		min = tmp;
	}

	mean = (max+min)*0.5;
	stdDev = (max-min)/6.0;
	do
	{
		u = ((double) rand() / ((double) RAND_MAX)) * 2.0 - 1.0;
		v = ((double) rand() / ((double) RAND_MAX)) * 2.0 - 1.0;
		s = u * u + v * v;
	}
	while( (s >= 1.0) || (s == 0.0) );

	s = sqrt(-2.0 * log(s) / s);
	return mean + stdDev * u * s;
}

#endif /* FUTIL_DISABLE_RANDOM_IMPLEMENTATION */
