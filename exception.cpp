/*
 * exception.cpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_EXCEPTION_IMPLEMENTATION

#include "exception.hpp"

#include <cstdarg>
#include <cstdio>
#include <stdexcept>

namespace futil
{
	/** Throws an standard exception with printf's style message */
	void throw_exception(const char* format, ...)
	{
		va_list args;
		va_start(args, format);
		char buffer[1024];
		vsprintf(buffer, format, args);
		std::string msg = std::string(buffer);
		va_end(args);
		throw std::runtime_error(msg.c_str());
	}
}

#endif /* FUTIL_DISABLE_EXCEPTION_IMPLEMENTATION */
