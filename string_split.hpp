/*
 * string_split.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STRING_SPLIT_HPP_
#define FUTIL_STRING_SPLIT_HPP_

#include <string>
#include <vector>

namespace futil
{
	/** Splits this string around matches of the given delimiter and stores the tokens in the given vector. Returns the vector itself. */
	std::vector<std::string>& split(std::vector<std::string>& vect, const std::string& str, char delim);

	/** Splits this string around matches of the given delimiter. Returns a vector containing the tokens. */
	std::vector<std::string> split(const std::string& str, char delim);

	/** Splits this string around matches of the given delimiter and stores the tokens in the given array. Returns the array itself. */
	std::string* split(std::string* array, const std::string& str, char delim);
}

#endif /* FUTIL_STRING_SPLIT_HPP_ */
