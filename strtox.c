/*
 * strtox.c
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2017 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */
#include <iso646.h>

#include "config.h"
#ifndef FUTIL_DISABLE_STRTOX_IMPLEMENTATION

#include "strtox.h"

#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <float.h>

// Generic strtoX-calling function skeleton
#define STRTOX(FUNC, FUNC_T, FUNC_T_MAX, T, T_MAX, STRTOX_MIN_CHECK, STRTOX_BASE)\
	char *endPtr;\
	const FUNC_T t = (errno = 0, FUNC(str, &endPtr STRTOX_BASE));\
	if(userEndPtr != NULL)\
		*userEndPtr = endPtr;\
	\
	if((errno == ERANGE and t == FUNC_T_MAX) or t > T_MAX)\
		return T_MAX;\
	\
	STRTOX_MIN_CHECK\
	\
	if (*str == '\0' or *endPtr != '\0')\
		return 0;\
	\
	else return (T) t;

// (strtoX parameter) Check for minimum boundaries
#define STRTOX_MIN_CHECKED(FUNC_MIN, T_MIN)\
	if((errno == ERANGE and t == FUNC_MIN) or t < T_MIN)\
		return T_MIN;

// (strtoX parameter) Do not check for mimimum boundaries
#define STRTOX_NO_MIN_CHECK

// (strtoX parameter) Pass base parameter
#define STRTOX_BASE_PASSED , base

// (strtoX parameter) Do not pass base parameter
#define STRTOX_NO_BASE

// macro that calls strtoX macro with the strtol function, ideal for signed integer types
#define STRTOL_RANGED(T, T_MIN, T_MAX) STRTOX(strtol,  long,          LONG_MAX,  T, T_MAX, STRTOX_MIN_CHECKED(LONG_MIN, T_MIN), STRTOX_BASE_PASSED)

// macro that calls strtoX macro with the strtoul function, ideal for unsigned integer types
#define STRTOUL_RANGED(T, T_MAX)       STRTOX(strtoul, unsigned long, ULONG_MAX, T, T_MAX, STRTOX_NO_MIN_CHECK,                 STRTOX_BASE_PASSED)

// macro that calls strtoX macro with the strtod function, ideal for floating-point types
#define STRTOD_RANGED(T, T_MIN, T_MAX) STRTOX(strtod,  double,        DBL_MAX,   T, T_MAX, STRTOX_MIN_CHECKED(DBL_MIN, T_MIN),  STRTOX_NO_BASE)

int strtoi(const char *str, char **userEndPtr, int base)
{
	STRTOL_RANGED(int, INT_MIN, INT_MAX)
}

unsigned strtou(const char *str, char **userEndPtr, int base)
{
	STRTOUL_RANGED(unsigned, UINT_MAX)
}

short strtoh(const char *str, char **userEndPtr, int base)
{
	STRTOL_RANGED(short, SHRT_MIN, SHRT_MAX)
}

unsigned short strtouh(const char *str, char **userEndPtr, int base)
{
	STRTOUL_RANGED(unsigned short, USHRT_MAX)
}

signed char strtoc(const char *str, char **userEndPtr, int base)
{
	STRTOL_RANGED(signed char, SCHAR_MIN, SCHAR_MAX)
}

unsigned char strtouc(const char *str, char **userEndPtr, int base)
{
	STRTOUL_RANGED(unsigned char, UCHAR_MAX)
}

#ifndef FUTIL_DISABLE_STRTOF_IMPLEMENTATION

float strtof(const char *str, char **userEndPtr)
{
	STRTOD_RANGED(float, FLT_MIN, FLT_MAX)
}

#endif /* FUTIL_DISABLE_STRTOF_IMPLEMENTATION */

#endif /* FUTIL_DISABLE_STRTOX_IMPLEMENTATION */
