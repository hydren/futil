/*
 * collection_actions.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_COLLECTION_ACTIONS_HPP_
#define FUTIL_COLLECTION_ACTIONS_HPP_

#include <algorithm>

/// Macro that should return the index of the first occurrence of the specified element in the given container (const), or -1 if it does not contain the element.
#define futil_index_of(collection, element) ((((std::find((collection).begin(), (collection).end(), (element)) - (collection).begin()) + 1) % ((collection).size() + 1)) - 1)

namespace futil
{
	/// Returns the first occurrence of a element (an iterator to it) in the given container, or collection.end() if it does not contain the element.
	template<typename CollectionType, typename Type>
	typename CollectionType::iterator position_of(CollectionType& collection, const Type& element)
	{
		return std::find(collection.begin(), collection.end(), element);
	}

	/// Returns the first occurrence of a element (an const iterator to it) in the given container (const), or collection.end() if it does not contain the element.
	template<typename CollectionType, typename Type>
	typename CollectionType::const_iterator position_of(const CollectionType& collection, const Type& element)
	{
		return std::find(collection.begin(), collection.end(), element);
	}

	/// Returns the index of the first occurrence of the specified element in the given container, or -1 if it does not contain the element.
	template<typename CollectionType, typename Type>
	int index_of(CollectionType& collection, const Type& element)
	{
		typename CollectionType::iterator it = std::find(collection.begin(), collection.end(), element);
		if(it == collection.end()) return -1;
		else return it - collection.begin();
	}

	/// Returns the index of the first occurrence of the specified element in the given container (const), or -1 if it does not contain the element.
	template<typename CollectionType, typename Type>
	int index_of(const CollectionType& collection, const Type& element)
	{
		typename CollectionType::const_iterator it = std::find(collection.begin(), collection.end(), element);
		if(it == collection.end()) return -1;
		else return it - collection.begin();
	}

	/// Returns true if the given container contains the specified element.
	template<typename CollectionType, typename Type>
	bool contains_element(CollectionType& collection, const Type& element)
	{
		typename CollectionType::iterator it = std::find(collection.begin(), collection.end(), element);
		return it != collection.end();
	}

	/// Returns true if the given container (const) contains the specified element.
	template<typename CollectionType, typename Type>
	bool contains_element(const CollectionType& collection, const Type& element)
	{
		typename CollectionType::const_iterator it = std::find(collection.begin(), collection.end(), element);
		return it != collection.end();
	}

	/// Erases a single instance of the specified element from the given container, if it is present. Returns true if an element was removed as a result of this call.
	template<typename CollectionType, typename Type>
	bool remove_element(CollectionType& collection, const Type& element)
	{
		typename CollectionType::iterator it = std::find(collection.begin(), collection.end(), element);
		if(it == collection.end()) return false;
		collection.erase(it); //First index. It may have more indexes depending on Type's operator==
		return true;
	}

	/// Variation of position_of() function to be used on containers of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns the first occurrence of a pointer, in the given container, to an element that is equal to the given element. Otherwise, it returns collection.end().
	template<typename CollectionType, typename Type>
	typename CollectionType::iterator position_of_ptr(CollectionType& collection, const Type& element)
	{
		typename CollectionType::iterator last = collection.end(), it = collection.begin();
		while (it != last) {
			if (**it == element) return it;
			++it;
		}
		return last;
	}

	/// Variation of position_of() function to be used on containers (const) of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns the first occurrence of a pointer, in the given container (const), to an element that is equal to the given element. Otherwise, it returns collection.end().
	template<typename CollectionType, typename Type>
	typename CollectionType::iterator position_of_ptr(const CollectionType& collection, const Type& element)
	{
		typename CollectionType::const_iterator last = collection.end(), it = collection.begin();
		while (it != last) {
			if (**it == element) return it;
			++it;
		}
		return last;
	}

	/// Variation of index_of() function to be used on containers of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns the index of a pointer, in the given container, to an element that is equal to the given element. Otherwise, it returns -1.
	template<typename CollectionType, typename Type>
	int index_of_ptr(CollectionType& collection, const Type& element)
	{
		typename CollectionType::iterator it = position_of(collection, element);
		if(it == collection.end()) return -1;
		else return it - collection.begin();
	}

	/// Variation of index_of() function to be used on containers (const) of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns the index of a pointer, in the given container (const), to an element that is equal to the given element. Otherwise, it returns -1.
	template<typename CollectionType, typename Type>
	int index_of_ptr(const CollectionType& collection, const Type& element)
	{
		typename CollectionType::const_iterator it = position_of(collection, element);
		if(it == collection.end()) return -1;
		else return it - collection.begin();
	}

	/// Variation of contains_element() function to be used on containers of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns true if the given container contains a pointer to an element that is equal to the given element.
	template<typename CollectionType, typename Type>
	bool contains_element_ptr(CollectionType& collection, const Type& element)
	{
		return index_of_ptr(collection, element) != -1;
	}

	/// Variation of contains_element() function to be used on containers (const) of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Returns true if the given container (const) contains a pointer to an element that is equal to the given element.
	template<typename CollectionType, typename Type>
	bool contains_element_ptr(const CollectionType& collection, const Type& element)
	{
		return index_of_ptr(collection, element) != -1;
	}

	/// Variation of remove_element() function to be used on containers of pointers of the element's type (i.e. std::vector<std::string*> collection, std::string element).
	/// Erases a single pointer, in the given container, to an element that is equal to the given element, if there is one. Returns true if a pointer was erased as a result of this call
	/// If alsoDelete is true, then the destructor is called for the object pointed by the erased pointer;
	template<typename CollectionType, typename Type>
	bool remove_element_ptr(CollectionType& collection, const Type& element, bool alsoDelete)
	{
		typename CollectionType::iterator it = std::find(collection.begin(), collection.end(), element);
		if(it == collection.end()) return false;
		collection.erase(it); //First index. It may have more indexes depending on Type's operator==
		if(alsoDelete) delete *it;
		return true;
	}
}

#endif /* FUTIL_COLLECTION_ACTIONS_HPP_ */
