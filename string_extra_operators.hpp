/*
 * string_extra_operators.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_STRING_EXTRA_OPERATORS_HPP_
#define FUTIL_STRING_EXTRA_OPERATORS_HPP_

#include <string>

#if __cplusplus >= 201103L
	#define FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED
#endif

std::string operator + (std::string a, bool b); /* Concatenate bool */
std::string operator + (bool a, std::string b); /* Concatenate bool, backwards version */

std::string operator + (std::string a, signed char b); /* Concatenate signed char */
std::string operator + (signed char a, std::string b); /* Concatenate signed char, backwards version */

std::string operator + (std::string a, unsigned char b); /* Concatenate unsigned char */
std::string operator + (unsigned char a, std::string b); /* Concatenate unsigned char, backwards version */

std::string operator + (std::string a, short b); /* Concatenate short */
std::string operator + (short a, std::string b); /* Concatenate short, backwards version */

std::string operator + (std::string a, unsigned short b); /* Concatenate unsigned short */
std::string operator + (unsigned short a, std::string b); /* Concatenate unsigned short, backwards version */

std::string operator + (std::string a, int b); /* Concatenate int */
std::string operator + (int a, std::string b); /* Concatenate int, backwards version */

std::string operator + (std::string a, unsigned b); /* Concatenate unsigned int */
std::string operator + (unsigned a, std::string b); /* Concatenate unsigned int, backwards version */

std::string operator + (std::string a, long b); /* Concatenate long */
std::string operator + (long a, std::string b); /* Concatenate long, backwards version */

std::string operator + (std::string a, unsigned long b); /* Concatenate unsigned long */
std::string operator + (unsigned long a, std::string b); /* Concatenate unsigned long, backwards version */

std::string operator + (std::string a, float b); /* Concatenate float */
std::string operator + (float a, std::string b); /* Concatenate float, backwards version */

std::string operator + (std::string a, double b); /* Concatenate double */
std::string operator + (double a, std::string b); /* Concatenate double, backwards version */

std::string operator + (std::string a, long double b); /* Concatenate long double */
std::string operator + (long double a, std::string b); /* Concatenate long double, backwards version */

// C99 / C++11 excluse types
#ifdef FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED

std::string operator + (std::string a, long long b); /* Concatenate long long */
std::string operator + (long long a, std::string b); /* Concatenate long long, backwards version */

std::string operator + (std::string a, unsigned long long b); /* Concatenate unsigned long long */
std::string operator + (unsigned long long a, std::string b); /* Concatenate unsigned long long, backwards version */

#endif /* FGEAL_STRING_EXTRA_OPERATORS_C99_TYPES_ENABLED */

std::string operator + (std::string a, void* b); /* Concatenate void* */
std::string operator + (void* a, std::string b); /* Concatenate void*, backwards version */

#endif /* FUTIL_STRING_EXTRA_OPERATORS_HPP_ */
