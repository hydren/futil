/*
 * language.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_LANGUAGE_HPP_
#define FUTIL_LANGUAGE_HPP_

//java hipster things
#define abstract =0
#define null NULL
#define extends :

#define by_ref &

#define FileInputStream std::ifstream
#define FileOutputStream std::ofstream

/// Aliases for unordered map and set (if c++11 is present)
#if __cplusplus >= 201103L
	#ifndef hash_map
		#define hash_map unordered_map
	#endif
	#ifndef hash_set
		#define hash_set unordered_set
	#endif
#endif

/// Useful foreach macro (prefer c++11 for-each if available)
#define foreach(TYPE, ELEMENT, COLLECTION_TYPE, COLLECTION)\
for(COLLECTION_TYPE::iterator ELEMENT##MACRO_TEMP_IT = (COLLECTION).begin(); ELEMENT##MACRO_TEMP_IT != (COLLECTION).end(); ++ELEMENT##MACRO_TEMP_IT)\
for(bool ELEMENT##MACRO_TEMP_B = true; ELEMENT##MACRO_TEMP_B;)\
for(TYPE ELEMENT = *(ELEMENT##MACRO_TEMP_IT); ELEMENT##MACRO_TEMP_B; ELEMENT##MACRO_TEMP_B = false)

/// Useful foreach macro (prefer c++11 for-each if available). Custom version for const qualified collections.
#define const_foreach(TYPE, ELEMENT, COLLECTION_TYPE, COLLECTION)\
for(COLLECTION_TYPE::const_iterator ELEMENT##MACRO_TEMP_IT = (COLLECTION).begin(); ELEMENT##MACRO_TEMP_IT != (COLLECTION).end(); ++ELEMENT##MACRO_TEMP_IT)\
for(bool ELEMENT##MACRO_TEMP_B = true; ELEMENT##MACRO_TEMP_B;)\
for(TYPE ELEMENT = *(ELEMENT##MACRO_TEMP_IT); ELEMENT##MACRO_TEMP_B; ELEMENT##MACRO_TEMP_B = false)

/// Get the iterator used with the given ELEMENT, inside a foreach or const_foreach macro scope
#define inner_iterator(ELEMENT) ELEMENT##MACRO_TEMP_IT

/// Predefined standard pimpl struct
#define encapsulated \
	struct Implementation; \
	Implementation *implementation

/// Macro to access pimpl'ed struct
#define encapsulation(PARENT_CLASS)\
	struct PARENT_CLASS::Implementation

#endif /* FUTIL_LANGUAGE_HPP_ */
