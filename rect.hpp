/*
 * rect.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_RECT_HPP_
#define FUTIL_RECT_HPP_

namespace futil
{
	/** POD struct that contains position and size of a box.
	 *  It has a method to check for intersection between other Rect's.
	 *  It would be named Rectangle but there is a internal name conflict in windows' and allegro's headers that prevents from using the name. For more info see: https://www.allegro.cc/forums/thread/612387 */
	struct Rect
	{
		float x, y, w, h;

		static inline float create(float x=0, float y=0, float w=0, float h=0)
		{
			const Rect tmp = {x, y, w, h};
			return tmp;
		}

		/// Returns true if this bounding box intersects with the given Rect.
		inline bool intersects(const Rect& box )
		{
			if(this->x + this->w <= box.x  //S1, S4, S7
			or this->x >= box.x + box.w    //S3, S6, S9
			or this->y + this->h <= box.y  //S1, S2, S3
			or this->y >= box.y + box.h    //S7, S8, S9
			) return false;
			else return true;
		}

		/** Returns a Rect that represents the intersection of this Rect with the given Rect.
		 * If there is no intersection, returns a empty Rect.
		 * In other words, you should check if the Rects intersects before trying to call this method, with the intersects() method. */
		inline Rect intersection(const Rect& box)
		{
			int x, y, h, w;
			if(!intersects(box)) return Rect();
			if(this->x <= box.x){
				x = box.x;
				if(this->w >= box.x + box.w)
					w = box.w;
				else
					w = this->w - (box.x - this->x);
			}
			else{
				x = this->x;
				if(box.w >= this->x + this->w)
					w = this->w;
				else
					w = box.w - (this->x - box.x);
			}
			if(this->y <= box.y){
				y = box.y;
				if(this->h >= box.h + box.y)
					h = box.h;
				else
					h = this->h - (box.y - this->y);
			}
			else{
				y = this->y;
				if(box.h >= this->h + this->y)
					h = this->h;
				else
					h = box.h - (this->y - box.y);
			}
			return Rect::create(x, y, w, h);
		}

		/** Returns the area of this Rect. The calculation is pretty silly: w*h (width x height) */
		inline int area()
		{
			return w*h; //you don't say
		}
	};
}

#endif /* FUTIL_RECT_HPP_ */
