/*
 * chrono.c
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_CHRONO_IMPLEMENTATION

#include "chrono.h"

#ifdef _WIN32
	#include <windows.h>
#elif __unix
	#include <sys/time.h>
	#include <unistd.h>
#endif

/** Returns the current time in milisseconds */
long current_time_ms()
{
	#ifdef _WIN32
		SYSTEMTIME st;
		GetSystemTime(&st);
		return st.wMilliseconds;
	#elif __unix
		struct timeval current;
		gettimeofday(&current, NULL);
		return current.tv_usec / 1000;
	#else
		return 0;
	#endif
}

/** Sleep for 'milliseconds', or until a signal arrives that is not blocked or ignored */
void rest(long milliseconds)
{
	#ifdef _WIN32
		Sleep(milliseconds);
	#elif __unix
		usleep(milliseconds*1000);
	#else
		return;
	#endif
}

#endif /* FUTIL_DISABLE_CHRONO_IMPLEMENTATION */
