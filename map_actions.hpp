/*
 * map_actions.hpp
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_MAP_ACTIONS_HPP_
#define FUTIL_MAP_ACTIONS_HPP_

#include <map>
#include <stdexcept>

namespace futil
{
	/** Attempt to return the value corresponding with the given key on the given map. If not found, returns 'valueIfAbsent'.
	 *  Note that the given map is NOT modified in the process. */
	template <typename KeyType, typename ValueType>
	ValueType coalesce(const std::map<KeyType, ValueType>& map1, KeyType key1, const ValueType& valueIfAbsent)
	{
		typedef std::map<KeyType, ValueType> MapType;

		typename MapType::const_iterator lb = map1.lower_bound(key1);

		if(lb != map1.end() and not map1.key_comp()(key1, lb->first))
			// key already exists
			return lb->second;

		else // the key does not exist in the map
			return valueIfAbsent;
	}

	/** Attempt to return the value corresponding with the given key on the given map.
	 *  If not found, inserts 'valueIfAbsent', associated with the given key and returns its value.
	 *  Note that the given map will be modified if the key isn't found. */
	template <typename KeyType, typename ValueType>
	ValueType& coalesce_in(std::map<KeyType, ValueType>& map1, KeyType key1, const ValueType& valueIfAbsent)
	{
		typedef std::map<KeyType, ValueType> MapType;

		typename MapType::iterator lb = map1.lower_bound(key1);

		if(lb != map1.end() and not map1.key_comp()(key1, lb->first))
		{
			// key already exists
			return lb->second;
		}
		else // the key does not exist in the map
		{
			// add it to the map and return value
			return map1.insert(lb, typename MapType::value_type(key1, valueIfAbsent))->second; // Use lb as a hint to insert so it can avoid another lookup
		}
	}

	/** Attempt to return the value corresponding with the given key on the given map. If not found, throws std::out_of_range.
	 *  Note that the given map is NOT modified in the process. */
	template <typename KeyType, typename ValueType>
	ValueType& get_value_at(const std::map<KeyType, ValueType>& map1, KeyType key1)
	{
		typedef std::map<KeyType, ValueType> MapType;

		typename MapType::const_iterator lb = map1.lower_bound(key1);

		if(lb != map1.end() and not map1.key_comp()(key1, lb->first))
			// key already exists
			return const_cast<ValueType&>(lb->second);

		else // the key does not exist in the map
			throw std::out_of_range("get_value_at: map does not contain specified key");
	}
}

#endif /* FUTIL_MAP_ACTIONS_HPP_ */
