/*
 * round.c
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "config.h"
#ifndef FUTIL_DISABLE_ROUND_IMPLEMENTATION

#include "round.h"
#ifdef FUTIL_ROUND_ENABLED

#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES
#endif  /* _USE_MATH_DEFINES */
#include <math.h>

	/** Computes the nearest integer value to x (in floating-point format), rounding halfway cases away from zero. If c++11 is enabled, the <cmath> version is used instead. */
	double round(double x)
	{
		return x < 0? ceil(x - 0.5) : floor(x + 0.5);
	}

#endif

#endif /* FUTIL_DISABLE_ROUND_IMPLEMENTATION */
