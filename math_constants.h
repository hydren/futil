/*
 * math_constants.h
 *
 * futil - Faruolo's personal C++ utility code
 *
 * Copyright (c) 2016 Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef FUTIL_MATH_CONSTANTS_H_
#define FUTIL_MATH_CONSTANTS_H_

#ifndef M_E
	#define M_E			2.71828182845904523536
#endif

#ifndef M_PI
	#define M_PI		3.14159265358979323846
#endif

#ifndef M_PI_2
	#define M_PI_2     	1.57079632679489661923
#endif

#ifndef M_PI_4
	#define M_PI_4     	0.785398163397448309616
#endif

#ifndef M_SQRT2
	#define M_SQRT2		1.41421356237309504880
#endif

#ifndef M_SQRT5
	#define M_SQRT5		2.23606797749978969640
#endif

#ifndef M_SQRT21
	#define M_SQRT21	4.58257569495584000658
#endif

#endif /* FUTIL_MATH_CONSTANTS_H_ */
